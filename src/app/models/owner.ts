export class Owner {
  userId: number;
  userType: string;
  reputation: number;
  acceptRate: number;
  profileImage: string;
  displayName: string;
  link: string;
}
