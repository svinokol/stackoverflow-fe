import { Owner } from './owner';

export class Question {
  title: string;
  body: string;
  link: string;
  questionId: number;
  isAnswered: boolean;
  tags: string[];
  owner: Owner;
  creationDate: number;
}
