import { Component, Input } from '../../../../node_modules/@angular/core';
import { QuestionService } from '../../services/question.service';
import { Question } from '../../models/question';

@Component({
  selector: 'app-question-component',
  templateUrl: 'question.component.html'
})
export class QuestionComponent {

  @Input()
  question: Question;

}
